package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext
public class PersonTest {

    @Autowired
    private PersonRepository repository;
    @Autowired
    private EntityManager em;


    @Test
    void test_01() {
//        assertTrue(true);
        repository.save(new Person(1L,"shuliang", "li"));
        em.flush();
        em.clear();
//        em.close();
        Optional<Person> person = repository.findById(1L);
        assertTrue(person.isPresent());
        assertThat(person.get().getFirstName()).isEqualTo("shuliang");
    }
}
